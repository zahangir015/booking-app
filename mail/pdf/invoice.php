<?php

use amnah\yii2\user\models\Profile;
use amnah\yii2\user\models\User;
use app\components\Utils;
use app\models\Company;

$baseUrl = Utils::get('webfront')['domain'];

$logo = Yii::getAlias('@app') . '/mail/pdf/img/D&D_Logo.JPG';
$sign = Yii::getAlias('@app') . '/mail/pdf/img/sign.jpg';
$company = Company::findOne(['id' => $invoice->companyId]);
$bank = \app\models\BankAccount::findOne(['id' => $invoice->bankId]);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Invoice</title>

    <style type="text/css">
        html {
            padding: 0;
            margin: 20px;
        }

        body {
            margin: 0;
            font-family: arial, sans-serif;
            font-size: 13px;
            color: #333;
            /*padding: 20px;*/
        }

        h1, h2, h3, h4, h5, h6, p, ul, ol, li {
            margin: 0;
            padding: 0;
        }

        p {
            line-height: 1.5;
        }

        ul {
            margin: 10px 0;
        }

        li {
            margin: 7px 25px;
        }

        table {
            width: 100%;
            border: 0;
        }

        .wrap {
            /*border: 1px solid gray;*/
            /*width: 800px;*/
        }

        .border-1x {
            border: 1px solid #ccc;
        }

        .border-in-1x {
            border: 1px solid #eee;
        }

        .bg-grey {
            background: #aaa;
        }

        .bg-light-grey {
            background: #ddd;
        }

        .padding-10 {
            padding: 10px;
        }

        .padding-70 {
            padding: 70px;
        }

        .padding-sm {
            padding: 7px;
        }

        .padding-xs {
            padding: 2px;
        }

        .margin-b10 {
            margin-bottom: 10px;
        }

        .margin-b5 {
            margin-bottom: 10px;
        }

        .margin-b15 {
            margin-bottom: 15px;
        }

        .header h1 {
            color: #f47b20
        }

        .nav-alike {
            color: white;
            padding: 2px 3px;
        }

        .bold {
            font-weight: bold;
        }

        .contact-info p {
            padding-bottom: 5px;
        }

        .contact-info p a {
            color: #2062ae
        }

        .red-text {
            color: red;
            text-transform: uppercase;
        }
    </style>

</head>
<body>
<div class="wrap">
    <div class="header padding-70">
        <table border="0" class="margin-b10">
            <tr>
                <td width="70%" valign="top">
                    <table border="0" style="text-align: left; margin-bottom: 15px;">
                        <tr>
                            <td class="bold" width="30%">Writer</td>
                            <td class="bold" width="70%">
                                : <?= Profile::findOne(['user_id' => (int)$invoice->createdBy])->full_name ?></td>
                        </tr>
                        <tr>
                            <td class="bold" width="30%">Email</td>
                            <td class="bold" width="70%">
                                : <?= User::findOne(['id' => (int)$invoice->createdBy])->email ?><br></td>
                        </tr>
                    </table>
                    <table border="0" style="text-align: left">
                        <tr>
                            <td class="bold" width="30%">Our Ref</td>
                            <td class="bold" width="70%">: <?= $invoice->ourRef ?></td>
                        </tr>
                        <tr>
                            <td class="bold" width="30%">Your Ref</td>
                            <td class="bold" width="70%">: <?= $invoice->yourRef; ?></td>
                        </tr>
                    </table>
                    <p class="bold" style="margin-top: 15px">
                        INVOICE NO: <?= $invoice->invoiceNumber ?>
                    </p><br>
                    <p class="margin-b5"><?= date('F j, Y', strtotime($invoice->date)) ?></p>
                    <p class="margin-b5">
                        <strong><?= $company->name ?></strong><br>
                        <?= $company->address ?><br>
                        <?= $company->city . ', ' . $company->country ?><br>
                        <?= $company->contact ?>
                    </p>
                </td>
                <td align="right" width="30%">
                    <p style="text-align: left;">
                        <img src="<?= $logo; ?>" alt="<?= $logo ?>" style="width: 100px; height: 100px;">
                    </p><br>
                    <p style="text-align: left;">
                        SINCE 1965<br>
                        <strong>
                            Advocates & Solicitors<br>
                            Trademark, Copyright &<br>
                            Patent Attorneys
                        </strong>
                    </p><br>
                    <p style="text-align: left;">
                        <strong>Incorporated Office:</strong><br>
                        Doulah House (Annexe)<br>
                        Plot 153/2, Road 2/2, Block -A<br>
                        Section-12, Mirpur<br>
                        Dhaka-1216, Bangladesh
                    </p><br>
                    <p style="text-align: left;">
                        <strong>Telecommunications:</strong><br>
                        Tel: 803 5538 & 900 3153<br>
                        Fax: 801 6442<br>
                        E-mail: doulah@doulah.com<br>
                        Website: www.doulah.com
                    </p>
                </td>
            </tr>
        </table>

        <p>Re:&nbsp;&nbsp; <strong><?= $invoice->tradeMark ?></strong></p>
        <div style="text-align: center">
            <h2>BILL/INVOICE</h2>
        </div>
        <table border="1" cellspacing="0" class="margin-b10">
            <thead class="bold" style="text-align: center;">
            <tr>
                <th width="80%">PARTICULARS</th>
                <th width="20%">AMOUNT (IN USD)</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $particulars = json_decode($invoice->particulars);
            $amounts = json_decode($invoice->amounts);
            foreach ($particulars as $key => $particular) {
                ?>
                <tr>
                    <td style="padding: 5px"><?= $particular ?></td>
                    <td style="text-align: center;"><?= number_format($amounts[$key],2) ?></td>
                </tr>
                <?php
            }
            ?>
            <tr>
                <td style="padding: 5px; text-align: center"><strong>Total</strong></td>
                <td style="text-align: center;"><?= number_format($invoice->total, 2) ?></td>
            </tr>
            </tbody>
        </table>

        <p class="margin-b10">In words - US DOLLAR <span style="text-transform: uppercase;"><?= Utils::convertNumber($invoice->total)?></span> ONLY.</p>

        <p class="margin-b10">Please remit the above net bill/invoice amount to our following Bank account at the earliest preferably by Swift transmission.</p>

        <table border="0" style="text-align: left; margin: 0 auto; width: 70%">
            <tr>
                <td width="30%">Name of Account</td>
                <td width="70%">: <strong><?= $bank->accountNumber ?></strong></td>
            </tr>
            <tr>
                <td width="30%">Number of Account</td>
                <td width="70%">: <?= $bank->accountNumber ?><br></td>
            </tr>
            <tr>
                <td width="30%">Name of Bank</td>
                <td width="70%">
                    : <?= $bank->bankName ?><br>
                    &nbsp;<?= $bank->address ?>
                </td>
            </tr>
            <tr>
                <td width="30%">Telephone</td>
                <td width="70%">: <?= $bank->phone ?><br></td>
            </tr>
            <tr>
                <td width="30%">Swift Sort Code</td>
                <td width="70%">: <?= $bank->swiftCode ?><br></td>
            </tr>
        </table>

        <div class="bold" style="margin-top: 10px;">
            <p>For Doulah & Doulah</p><br>
            <img src="<?= $sign; ?>" alt="<?= $sign ?>" style="width: 115px; height: 31px;"><br>
            <p>ABH Shamsud Doulah</p>
            <p>Senior Partner</p>
        </div>
    </div>
</body>
</html>
