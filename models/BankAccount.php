<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%bank_account}}".
 *
 * @property int $id
 * @property string $accountName
 * @property string $accountNumber
 * @property string $bankName
 * @property string $address
 * @property string $phone
 * @property string $swiftCode
 * @property string $createdAt
 * @property int $createdBy
 * @property string $updatedAt
 * @property int $updatedBy
 */
class BankAccount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%bank_account}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['accountName', 'accountNumber', 'bankName', 'createdAt', 'createdBy'], 'required', 'on' => 'create'],
            [['accountName', 'accountNumber', 'bankName', 'updatedAt', 'updatedBy'], 'required', 'on' => 'update'],
            [['address'], 'string'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['createdBy', 'updatedBy'], 'integer'],
            [['accountName', 'accountNumber', 'bankName', 'phone', 'swiftCode'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'accountName' => Yii::t('app', 'Account Name'),
            'accountNumber' => Yii::t('app', 'Account Number'),
            'bankName' => Yii::t('app', 'Bank Name'),
            'address' => Yii::t('app', 'Address'),
            'phone' => Yii::t('app', 'Phone'),
            'swiftCode' => Yii::t('app', 'Swift Code'),
            'createdAt' => Yii::t('app', 'Created At'),
            'createdBy' => Yii::t('app', 'Created By'),
            'updatedAt' => Yii::t('app', 'Updated At'),
            'updatedBy' => Yii::t('app', 'Updated By'),
        ];
    }
}
