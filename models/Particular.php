<?php

namespace app\models;

use amnah\yii2\user\models\Profile;
use Yii;

/**
 * This is the model class for table "{{%particular}}".
 *
 * @property int $id
 * @property string $title
 * @property int $status
 * @property string $createdAt
 * @property int $createdBy
 * @property string $updatedAt
 * @property int $updatedBy
 */
class Particular extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%particular}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'createdAt', 'createdBy'], 'required', 'on' => ['create']],
            [['title', 'updatedAt', 'updatedBy'], 'required', 'on' => ['update']],
            [['status', 'createdBy', 'updatedBy'], 'integer'],
            [['createdAt', 'updatedAt'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'status' => Yii::t('app', 'Status'),
            'createdAt' => Yii::t('app', 'Created At'),
            'createdBy' => Yii::t('app', 'Created By'),
            'updatedAt' => Yii::t('app', 'Updated At'),
            'updatedBy' => Yii::t('app', 'Updated By'),
        ];
    }

    public function getProfile(){
        return $this->hasOne(Profile::className(), ['user_id' => 'createdBy']);
    }
}
