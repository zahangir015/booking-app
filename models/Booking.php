<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%booking}}".
 *
 * @property int $id
 * @property string $destination
 * @property string $carNumber
 * @property string $bookingTime
 * @property string $returnTime
 * @property string $pickupFrom
 * @property int $passengers
 * @property string $createdAt
 * @property int $createdBy
 * @property string $updatedAt
 * @property int $updatedBy
 */
class Booking extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%booking}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['destination', 'carNumber', 'bookingTime', 'returnTime', 'createdAt', 'createdBy'], 'required'],
            [['destination', 'carNumber', 'pickupFrom'], 'string'],
            [['bookingTime', 'carNumber'], 'checkDate'],
            [['bookingTime', 'returnTime', 'createdAt', 'updatedAt'], 'safe'],
            [['passengers', 'createdBy', 'updatedBy'], 'integer'],
        ];
    }

    public function checkDate($attribute, $params)
    {
        if ($this->isNewRecord){
            $thereIsAnOverlapping = Booking::find()->where(
                ['AND',
                    ['carNumber' => $this->carNumber],
                    ['<=', 'bookingTime', $this->returnTime],
                    ['>=', 'returnTime', $this->bookingTime]
                ])->exists();
        }else{
            $thereIsAnOverlapping = Booking::find()->where(
                ['AND',
                    ['carNumber' => $this->carNumber],
                    ['<=', 'bookingTime', $this->returnTime],
                    ['>=', 'returnTime', $this->bookingTime]
                ])->andWhere(['!=', 'id', $this->id])->exists();
        }


        if ($thereIsAnOverlapping) {
            $this->addError($attribute, 'This car is busy for this requested booking date time, selcet another car or change date time.');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'destination' => Yii::t('app', 'Destination'),
            'carNumber' => Yii::t('app', 'Car Number'),
            'bookingTime' => Yii::t('app', 'Booking Time'),
            'returnTime' => Yii::t('app', 'Return Time'),
            'pickupFrom' => Yii::t('app', 'Pickup From'),
            'passengers' => Yii::t('app', 'Passengers'),
            'createdAt' => Yii::t('app', 'Created At'),
            'createdBy' => Yii::t('app', 'Created By'),
            'updatedAt' => Yii::t('app', 'Updated At'),
            'updatedBy' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     * @return BookingQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BookingQuery(get_called_class());
    }

    public function createdBy(){
        return $this->hasOne(User::className(), ['id' => 'createdBy']);
    }
}
