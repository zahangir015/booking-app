<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Inbill;

/**
 * InbillSearch represents the model behind the search form of `app\models\Inbill`.
 */
class InbillSearch extends Inbill
{
    public $company;
    public $bank;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'companyId', 'bankId', 'createdBy', 'updatedBy'], 'integer'],
            [['ourRef', 'yourRef', 'invoiceNumber', 'date', 'tradeMark', 'particulars', 'amounts', 'currency', 'createdAt', 'updatedAt', 'company', 'bank'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Inbill::find();

        // add conditions that should always apply
        $query->joinWith(['company', 'bank']);

        if (!Yii::$app->user->can('admin')){
            $query->where(['createdBy' => Yii::$app->user->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['company'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['company.name' => SORT_ASC],
            'desc' => ['company.name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['bank'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['bank_account.bankName' => SORT_ASC],
            'desc' => ['bank_account.bankName' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'companyId' => $this->companyId,
            'total' => $this->total,
            'bankId' => $this->bankId,
            'createdAt' => $this->createdAt,
            'createdBy' => $this->createdBy,
            'updatedAt' => $this->updatedAt,
            'updatedBy' => $this->updatedBy,
        ]);

        $query->andFilterWhere(['like', 'ourRef', $this->ourRef])
            ->andFilterWhere(['like', 'yourRef', $this->yourRef])
            ->andFilterWhere(['like', 'invoiceNumber', $this->invoiceNumber])
            ->andFilterWhere(['like', 'tradeMark', $this->tradeMark])
            ->andFilterWhere(['like', 'particulars', $this->particulars])
            ->andFilterWhere(['like', 'amounts', $this->amounts])
            ->andFilterWhere(['like', 'currency', $this->currency])
            ->andFilterWhere(['like', 'company.name', $this->company])
            ->andFilterWhere(['like', 'bank_account.bankName', $this->bank]);

        return $dataProvider;
    }
}
