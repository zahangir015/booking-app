<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%inbill}}".
 *
 * @property int $id
 * @property string $ourRef
 * @property string $yourRef
 * @property string $invoiceNumber
 * @property string $date
 * @property int $companyId
 * @property string $tradeMark
 * @property string $particulars
 * @property string $amounts
 * @property double $total
 * @property string $currency
 * @property int $bankId
 * @property int $status
 * @property string $createdAt
 * @property int $createdBy
 * @property string $updatedAt
 * @property int $updatedBy
 */
class Inbill extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%inbill}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ourRef', 'yourRef', 'invoiceNumber', 'date', 'companyId', 'tradeMark', 'particulars', 'amounts', 'total', 'currency', 'bankId', 'createdAt', 'createdBy'], 'required'],
            [['date', 'createdAt', 'updatedAt'], 'safe'],
            [['companyId', 'bankId', 'createdBy', 'updatedBy', 'status'], 'integer'],
            [['particulars', 'amounts'], 'string'],
            [['total'], 'number'],
            [['ourRef', 'yourRef', 'invoiceNumber', 'tradeMark', 'currency'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'ourRef' => Yii::t('app', 'Our Ref'),
            'yourRef' => Yii::t('app', 'Your Ref'),
            'invoiceNumber' => Yii::t('app', 'Invoice Number'),
            'date' => Yii::t('app', 'Date'),
            'companyId' => Yii::t('app', 'Company'),
            'tradeMark' => Yii::t('app', 'Trade Mark'),
            'particulars' => Yii::t('app', 'Particulars'),
            'amounts' => Yii::t('app', 'Amounts'),
            'total' => Yii::t('app', 'Total'),
            'currency' => Yii::t('app', 'Currency'),
            'bankId' => Yii::t('app', 'Bank ID'),
            'createdAt' => Yii::t('app', 'Created At'),
            'createdBy' => Yii::t('app', 'Created By'),
            'updatedAt' => Yii::t('app', 'Updated At'),
            'updatedBy' => Yii::t('app', 'Updated By'),
        ];
    }

    public function getCompany(){
        return $this->hasOne(Company::className(), ['id' => 'companyId']);
    }

    public function getBank(){
        return $this->hasOne(BankAccount::className(), ['id' => 'bankId']);
    }
}
