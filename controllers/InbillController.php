<?php

namespace app\controllers;

use app\components\PdfGen;
use Yii;
use app\models\Inbill;
use app\models\InbillSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InbillController implements the CRUD actions for Inbill model.
 */
class InbillController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Inbill models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InbillSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Inbill model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Inbill model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Inbill();

        if (Yii::$app->request->isPost) {

            if ($model->load(Yii::$app->request->post())) {
                $model->particulars = json_encode(Yii::$app->request->post('Inbill')['particulars']);
                $model->amounts = json_encode(Yii::$app->request->post('Inbill')['amounts']);
                $model->createdAt = date('Y-m-d H:i:s');
                $model->createdBy = Yii::$app->user->id;

                if ($model->save()) {
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Inbill model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            dd(Yii::$app->request->post());
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Inbill model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Mail an existing Inbill model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionMail($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isPost) {
            $emailContent = Yii::$app->request->post();

            $pdfContent = PdfGen::attachment(['invoice' => $model], 'invoice');

            $mail = Yii::$app->mailer->compose();
            $mail->setFrom('noreply@travelbookingbd.com')
                ->setTo($emailContent['to'])
                ->setSubject('Invoice - ' . $emailContent['invoiceNumber'])
                ->setHtmlBody($emailContent['body'])
                ->attach($pdfContent);

            if ($mail->send()) {
                Yii::$app->session->getFlash('success', 'Mail sent successfully');
                return $this->redirect('index');
            }
        }

        return $this->render('mail', [
            'model' => $model,
        ]);
    }

    /**
     * Finds the Inbill model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Inbill the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Inbill::findOne(['id' => (int)$id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionAddParticular($row)
    {
        $model = new Inbill();
        return $this->renderPartial('particular', [
            'model' => $model,
            'row' => $row
        ]);
    }

    public function actionDownload($id)
    {

        $invoice = $this->findModel($id);
        $fileName = 'invoice';
        PdfGen::invoice(['invoice' => $invoice], $fileName);
    }
}
