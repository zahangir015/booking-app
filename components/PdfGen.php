<?php
/**
 * Created by PhpStorm.
 * User: lenin
 * Date: 4/17/16
 * Time: 5:29 PM
 */

namespace app\components;

use Dompdf\Dompdf;
use Yii;

class PdfGen {
    /**
     * generates the booking voucher for customer
     *
     * @param $hotelBookingHistory
     * @return string
     */

    public static function makeMoneyReceipt($data,$fileName)
    {
        define('DOMPDF_ENABLE_AUTOLOAD', false);
        define('DOMPDF_ENABLE_CSS_FLOAT', true);
        $pdfTemplate = '@app/mail/pdf/'.$fileName;

        $html = Yii::$app->view->render($pdfTemplate, $data);

        $dompdf = new Dompdf();
        $assets = Yii::getAlias('@app') . "/pdf/assets/";
        $dompdf->set_base_path($assets);

        $html = preg_replace('/>\s+</', "><", $html);
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($fileName,array('Attachment'=>1));
    }

    public static function invoice($data,$fileName)
    {
        define('DOMPDF_ENABLE_AUTOLOAD', false);
        define('DOMPDF_ENABLE_CSS_FLOAT', true);
        $pdfTemplate = '@app/mail/pdf/'.$fileName;

        $html = Yii::$app->view->render($pdfTemplate, $data);

        $dompdf = new Dompdf();
        $assets = Yii::getAlias('@app') . "/pdf/assets/";
        $dompdf->set_base_path($assets);

        $html = preg_replace('/>\s+</', "><", $html);
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($fileName,array('Attachment'=>1));
    }

    public static function attachment($data,$fileName)
    {
        define('DOMPDF_ENABLE_AUTOLOAD', false);
        define('DOMPDF_ENABLE_CSS_FLOAT', true);
        $pdfTemplate = '@app/mail/pdf/'.$fileName;
        $html = Yii::$app->view->render($pdfTemplate, $data);
        $assets = Yii::getAlias('@app') . "/pdf/assets/";
        $dompdf = new Dompdf();
        $dompdf->set_base_path($assets);
        $dompdf->load_html($html);
        $dompdf->render();
        $pdfContent = $dompdf->output();

        $filename = 'invoice-' . uniqid() . '.pdf';
        $path = Utils::alias('@app').'/mail/pdf/tmp';
        Utils::checkDir($path);
        $filePath = $path .'/'. $filename;

        file_put_contents($filePath, $pdfContent);
        return $filePath;
    }

} 