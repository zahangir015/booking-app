<?php
/**
 * Created by PhpStorm.
 * User: sajib
 * Date: 6/17/15
 * Time: 5:49 PM
 */

namespace app\components;

use common\modules\leninhasda\options\models\Options;
use DateTime;
use DateTimeZone;
use Yii;
use yii\web\Response;

class Utils
{
    /**
     * handles json responses
     *
     * @param $data
     * @param int $statusCode
     * @return mixed
     */

    const IMAGE_ORIGINAL = 'original';
    const IMAGE_LARGE = 'large';
    const IMAGE_MEDIUM = 'medium';
    const IMAGE_SMALL = 'small';
    const IMAGE_THUMB = 'thumb';
    const IMAGE_LOW = 'low';

    public static function json_response($data, $statusCode = 200)
    {
        if (200 != $statusCode) {
            Yii::$app->response->setStatusCode($statusCode);
        }
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $data;
    }

    /**
     * @param $name
     * @return bool|string
     */
    public static function alias($name)
    {
        return Yii::getAlias($name);
    }

    /**
     * a short hand method to get params value
     *
     * @param $key
     * @return mixed
     */
    public static function get($key)
    {
        if ( ! $key) {
            return false;
        }
        if ( ! isset(Yii::$app->params[$key])) {
            return false;
        }

        return Yii::$app->params[$key];
    }

    /**
     * @param $filename
     * @return bool
     */
    public static function isFile($filename)
    {
        return is_file($filename);
    }

    /**
     * @param $dirname
     * @return bool
     */
    public static function isDir($dirname)
    {
        return is_dir($dirname);
    }

    /**
     * @param $filename
     * @return bool
     */
    public static function exits($filename)
    {
        return file_exists($filename);
    }

    /**
     * @param $var
     * @param bool $flag
     */
    public static function debug($var, $flag = true)
    {
        echo '<pre>';
        if ($flag) {
            print_r($var);
        } else {
            var_dump($var);
        }
        echo '</pre>';
        die();
    }

    public static function dump($var)
    {
        \yii\helpers\VarDumper::dump($var);
        die();
    }

    public static function getUploadPath()
    {
        return self::alias('@uploads');
    }

    public static function getAllowedTypes()
    {
        $uploadOptions = self::get('uploadOptions');

        return $uploadOptions['allowedTypes'];
    }

    public static function getAllowedImageTypes()
    {
        $uploadOptions = self::get('uploadOptions');
        $imageExt = $uploadOptions['imageTypes'];
        $imageTypes = $uploadOptions['allowedTypes'];
        foreach ($imageTypes as $key => $type) {
            if (!in_array($key, $imageExt)) {
                unset($imageTypes[$key]);
            }
        }

        return $imageTypes;
    }

    /**
     * @return mixed
     */
    public static function getMaxUploadCount()
    {
        $uploadOptions = self::get('uploadOptions');
        return $uploadOptions['maxFileCount'];
    }

    /**
     * @return mixed
     */
    public static function getMaxUploadSize()
    {
        $uploadOptions = self::get('uploadOptions');
        return $uploadOptions['maxFileSize'];
    }

    /**
     * @param $file
     * @param string $type
     * @return bool
     */
    public static function validateFile($file, $type = 'file')
    {
        if (!$file || ! self::exits($file->tempName)) {
            return false;
        }

        $maxSize = self::getMaxUploadSize();
        if( $file->size > $maxSize ) {
            return false;
        }

        $uploadOptions = self::get('uploadOptions');
        $key = 'allowed' . ucfirst($type) . 'Types';
        $allowedTypes = $uploadOptions[$key];

        switch ($type) {
            case "image":
                return self::validateImageFile($file, $allowedTypes);
                break;
            case "file":
                return self::validateFileTypes($file, $allowedTypes);
                break;
            default:
                return false;
                break;
        }

        // return false;
    }

    /**
     * @param $file
     * @param $allowedTypes
     * @return bool
     */
    protected static function validateImageFile($file, $allowedTypes)
    {
        //$isValid = self::validateFileTypes($file, $allowedTypes);
        $isValid = true;
        //if( $isValid ) {
        $ext = $file->extension;
        $fileInfo = getimagesize( $file->tempName );
        if( ! isset($fileInfo[0]) || ! is_numeric($fileInfo[0]) || $fileInfo[0] <= 0
            || ! isset($fileInfo[0]) || ! is_numeric($fileInfo[0]) || $fileInfo[0] <= 0
            || $fileInfo['mime'] !== $allowedTypes[$ext] ) {
            $isValid = false;
        }
        //}
        return $isValid;
    }

    /**
     * @param $file
     * @param $allowedTypes
     * @return bool
     */
    protected static function validateFileTypes($file, $allowedTypes)
    {
        $isValid = true;
        // $ext = end((explode('.', $file->name)));
        $ext = $file->extension;

        if( $file->error  !== UPLOAD_ERR_OK
            || ! isset($allowedTypes[$ext])
            || $file->type !== $allowedTypes[$ext] ) {
            $isValid = false;
        }

        return $isValid;
    }

    /**
     * @param $name
     * @param $size
     * @return string
     */
    public static function imageLink($name, $size)
    {
        $base = self::get('baseImageUrl');

        return $base . $size . '/' . $name;
    }

    /**
     * @param $path
     */
    public static function checkDir($path)
    {
        if( is_array($path) ) {
            foreach ($path as $p) {
                self::checkDir($p);
            }
        } else {
            // check if upload dir exists
            if( ! file_exists($path) ) {
                mkdir($path, 0755);
            }
        }
    }

    /**
     * @param $path
     */
    public static function checkFile($path)
    {
        return file_exists($path);
    }

    public static function move($source, $destination)
    {
        if( ! self::exits($source) ) {
            return false;
        }
        return rename ( $source , $destination);
    }

    /**
     * @return string
     */
    public static function getRandomName()
    {
        return Yii::$app->security->generateRandomString();
    }

    public static function getApiUrl($endPoint)
    {
        $apiURL = Options::get('api_URL');
        $apiVersion = Options::get('api_version');
        return $apiURL . '/' . $apiVersion .'/'. $endPoint;
    }

    public static function base64_url_encode($input) {
        return strtr(base64_encode($input), '+/=', '-_,');
    }

    public static function base64_url_decode($input) {
        return base64_decode(strtr($input, '-_,', '+/='));
    }

    /**
     * returns date &| time in given format
     *
     * @param $format
     * @param null $time
     * @return string
     */
    public static function date($format, $time = null)
    {
        if( $time == null ) {
            $time = "now";
        }
        if( ! $format || ! is_string($format) ) {
            $format = 'Y-m-d';
        }
        $datetime = new DateTime($time);
        return $datetime->format($format);
    }

    /**
     * return sql date time
     * @param null $time
     * @return string
     */
    public static function sqlDatetime($time = null)
    {
        return self::date('Y-m-d H:i:s', $time);
    }

    /**
     * return sql date time
     * @param null $time
     * @return string
     */
    public static function humanDate($time = null)
    {
        return self::date('F d, Y', $time);
    }

    /**
     * convert to local time aka BD time
     * @param $originalDateTime
     * @param string $localTimeZone
     */
    public static function localDatetime($originalDateTime, $localTimeZone = 'Asia/Dhaka')
    {
        $systemTimeZone = 'UTC';
        $originalTimeZone = new DateTimeZone( $systemTimeZone );

        $datetime = new DateTime($originalDateTime, $originalTimeZone);

        $targetTimeZone = new DateTimeZone( $localTimeZone );
        $datetime->setTimeZone($targetTimeZone);

        $format = 'Y-m-d h:i:s a';
        return $datetime->format($format);;
    }

    /**
     * converts array to object
     *
     * @param $arr
     * @return mixed
     */
    public static function toObject($arr)
    {
        return json_decode(json_encode($arr));
    }

    /**
     * logs error message
     *
     * @param $var
     */
    public static function log($var)
    {
        $msg = print_r($var, true);
        Yii::info($msg);
    }

    public static function unlink($file)
    {
        if( self::checkFile($file) ) {
            return unlink($file);
        }
        return false;
    }

    /*
     * transfer, package & tour code generator
     * @param $str
     * @return mixed
     */

    public static  function tpt_code($str=''){
        return 'TBBD'.Yii::$app->user->id.$str.Utils::date('YmdHis');
    }

    public static function encode($var)
    {
        return json_encode($var);
    }

    public static function decode($var)
    {
        return json_decode($var);
    }

    /*
     * number to word convert
     */

    public static function convertNumber($number)
    {
        if ( is_numeric( $number ) && strpos( $number, '.' ) === false ){
            $number  = number_format((float)$number, 2, '.', '');
        }

        list($integer, $fraction) = explode(".", (string)$number);

        $output = "";

        if ($integer{0} == "-")
        {
            $output = "negative ";
            $integer    = ltrim($integer, "-");
        }
        else if ($integer{0} == "+")
        {
            $output = "positive ";
            $integer    = ltrim($integer, "+");
        }

        if ($integer{0} == "0")
        {
            $output .= "zero";
        }
        else
        {
            $integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
            $group   = rtrim(chunk_split($integer, 3, " "), " ");
            $groups  = explode(" ", $group);

            $groups2 = array();
            foreach ($groups as $g)
            {
                $groups2[] = self::convertThreeDigit($g{0}, $g{1}, $g{2});
            }

            for ($z = 0; $z < count($groups2); $z++)
            {
                if ($groups2[$z] != "")
                {
                    $output .= $groups2[$z] . self::convertGroup(11 - $z) . (
                        $z < 11
                        && !array_search('', array_slice($groups2, $z + 1, -1))
                        && $groups2[11] != ''
                        && $groups[11]{0} == '0'
                            ? " and "
                            : ", "
                        );
                }
            }

            $output = rtrim($output, ", ");
        }

        if ($fraction > 0)
        {
            $output .= " point";
            for ($i = 0; $i < strlen($fraction); $i++)
            {
                $output .= " " . self::convertDigit($fraction{$i});
            }
        }

        return $output;
    }

    public static function convertGroup($index)
    {
        switch ($index)
        {
            case 11:
                return " decillion";
            case 10:
                return " nonillion";
            case 9:
                return " octillion";
            case 8:
                return " septillion";
            case 7:
                return " sextillion";
            case 6:
                return " quintrillion";
            case 5:
                return " quadrillion";
            case 4:
                return " trillion";
            case 3:
                return " billion";
            case 2:
                return " million";
            case 1:
                return " thousand";
            case 0:
                return "";
        }
    }

    public static function convertThreeDigit($digit1, $digit2, $digit3)
    {
        $buffer = "";

        if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
        {
            return "";
        }

        if ($digit1 != "0")
        {
            $buffer .= self::convertDigit($digit1) . " hundred";
            if ($digit2 != "0" || $digit3 != "0")
            {
                $buffer .= " and ";
            }
        }

        if ($digit2 != "0")
        {
            $buffer .= self::convertTwoDigit($digit2, $digit3);
        }
        else if ($digit3 != "0")
        {
            $buffer .= self::convertDigit($digit3);
        }

        return $buffer;
    }

    public static function convertTwoDigit($digit1, $digit2)
    {
        if ($digit2 == "0")
        {
            switch ($digit1)
            {
                case "1":
                    return "ten";
                case "2":
                    return "twenty";
                case "3":
                    return "thirty";
                case "4":
                    return "forty";
                case "5":
                    return "fifty";
                case "6":
                    return "sixty";
                case "7":
                    return "seventy";
                case "8":
                    return "eighty";
                case "9":
                    return "ninety";
            }
        } else if ($digit1 == "1")
        {
            switch ($digit2)
            {
                case "1":
                    return "eleven";
                case "2":
                    return "twelve";
                case "3":
                    return "thirteen";
                case "4":
                    return "fourteen";
                case "5":
                    return "fifteen";
                case "6":
                    return "sixteen";
                case "7":
                    return "seventeen";
                case "8":
                    return "eighteen";
                case "9":
                    return "nineteen";
            }
        } else
        {
            $temp = self::convertDigit($digit2);
            switch ($digit1)
            {
                case "2":
                    return "twenty-$temp";
                case "3":
                    return "thirty-$temp";
                case "4":
                    return "forty-$temp";
                case "5":
                    return "fifty-$temp";
                case "6":
                    return "sixty-$temp";
                case "7":
                    return "seventy-$temp";
                case "8":
                    return "eighty-$temp";
                case "9":
                    return "ninety-$temp";
            }
        }
    }

    public static function convertDigit($digit)
    {
        switch ($digit)
        {
            case "0":
                return "zero";
            case "1":
                return "one";
            case "2":
                return "two";
            case "3":
                return "three";
            case "4":
                return "four";
            case "5":
                return "five";
            case "6":
                return "six";
            case "7":
                return "seven";
            case "8":
                return "eight";
            case "9":
                return "nine";
        }
    }
}