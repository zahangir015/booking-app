/**
 * Created by Zahangir on 6/21/2017.
 */

$(function () {

    /**
     * Prerequisite section
     */

    $('#addParticulars').click(function (e) {
        var row = $(this).attr('data-click');
        ajaxCall(row);
    });

    $('#removeParticulars').click(function (e) {
        var row = $(this).attr('data-click');
        var id = '#element' + parseInt(row);

        if ($(id).length == 0) {
            alert('Not available!');
        } else {
            $('#element' + parseInt(row)).remove();
            $('#removeParticulars').attr('data-click', (parseInt(row) - 1));
            $('#addParticulars').attr('data-click', (parseInt($('#addParticulars').attr('data-click')) - 1));
        }

    });

    function ajaxCall(row) {
        $.ajax({
            url: ajaxUrl,
            type: 'get',
            data: {
                row: row,
            },
            dataType: 'html',
            success: function (data) {
                $('#particular').append(data);
                $('#addParticulars').attr('data-click', (parseInt(row) + 1));
                $('#removeParticulars').attr('data-click', (parseInt($('#addParticulars').attr('data-click')) - 1));
            },
            error: function () {
                alert('Error happend!');
            }
        });
    }

    $(document).on('change', ".amount", function () {
        var sum = 0;

        $('.amount').each(function(){
            if(!isNaN(parseFloat($(this).val()))){
                sum += parseFloat($(this).val());
            }
        });

        $('#inbill-total').val(sum);

    });

});