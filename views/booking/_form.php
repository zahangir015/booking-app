<?php

use kartik\datetime\DateTimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Booking */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booking-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'destination')->dropDownList(['Location 1' => 'Location 1', 'Location 2' => 'Location 2', 'Location 3' => 'Location 3', 'Location 4' => 'Location 4',], ['prompt' => '']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'carNumber')->dropDownList(['Car 1' => 'Car 1', 'Car 2' => 'Car 2', 'Car 3' => 'Car 3',], ['prompt' => '']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'bookingTime')->widget(DateTimePicker::className(), [
                //'value' => '01/04/2005 08:17',
                'removeButton' => false,
                'pickerButton' => ['icon' => 'time'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/mm/dd hh:ii'
                ]
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'returnTime')->widget(DateTimePicker::className(), [
                //'value' => '01/04/2005 08:17',
                'removeButton' => false,
                'pickerButton' => ['icon' => 'time'],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy/mm/dd hh:ii'
                ]
            ]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'pickupFrom')->dropDownList(['Location 1' => 'Location 1', 'Location 2' => 'Location 2', 'Location 3' => 'Location 3', 'Location 4' => 'Location 4',], ['prompt' => '']) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'passengers')->textInput(['type' => 'number']) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
