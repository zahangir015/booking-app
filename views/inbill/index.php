<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\InbillSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Inbills');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inbill-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Inbill'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'ourRef',
            'yourRef',
            'invoiceNumber',
            [
                'attribute' => 'company',
                'label' => 'Company Name',
                'value' => function($model){
                    return $model->company->name.' - '.$model->company->country;
                },
            ],
            [
                'attribute' => 'bank',
                'label' => 'Bank Name',
                'value' => function($model){
                    return $model->bank->bankName.' - '.$model->bank->accountName;
                },
            ],

            //'tradeMark',
            //'particulars:ntext',
            //'amounts:ntext',
            'total',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function($model){
                    return ($model->status) ? 'Paid' : 'Unpaid';
                },
            ],
            'date',
            //'currency',

            //'createdAt',
            //'createdBy',
            //'updatedAt',
            //'updatedBy',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {receipt} {mail} {delete}',
                'buttons'=>[
                    'view' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['view', 'id' => $key], [
                            'title' => Yii::t('app', 'Details'),
                            'class' => 'btn btn-default btn-xs custom_button',
                            'data-pjax' => '0',
                        ]);
                    },
                    'update' => function ($url, $model, $key) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $key], [
                            'title' => Yii::t('app', 'Edit'),
                            'class' => 'btn btn-default btn-xs custom_button',
                            'data-pjax' => '0',
                        ]);
                    },
                    'receipt' => function ($url, $model, $key) {

                        return Html::a('<span class="glyphicon glyphicon-download-alt"></span>', ['download', 'id' => $key], [
                            'title' => Yii::t('app', 'Money Receipt'),
                            'class' => 'btn btn-default btn-xs custom_button',
                            'target' => '_blank',
                            'data-pjax' => '0',
                        ]);
                    },
                    'mail' => function ($url, $model, $key) {

                        return Html::a('<span class="glyphicon glyphicon-envelope"></span>', ['mail', 'id' => $key], [
                            'title' => Yii::t('app', 'Money Receipt'),
                            'class' => 'btn btn-default btn-xs custom_button',
                            'data-pjax' => '0',
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $key],
                            [
                                'title' => Yii::t('app', 'Delete'),
                                'data-ajax' => '0',
                                'data-method' => 'POST',
                                'data-confirm' => 'Are you sure you want to delete this item?',
                                'class' => 'btn btn-default btn-xs custom_button',
                            ]);
                    }
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
