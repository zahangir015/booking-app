<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Inbill */

$this->title = Yii::t('app', 'Create Inbill');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inbills'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inbill-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
