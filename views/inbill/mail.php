<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Inbill */

$this->title = Yii::t('app', 'Email Inbill: {nameAttribute}', [
    'nameAttribute' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inbills'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

?>
<div class="inbill-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="inbill-mail-form">

        <?php
        if ($model->hasErrors()) {
            foreach ($model->errors as $error) {
                echo '<div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                ' . implode('<br>', $error) . '
              </div>';
            }
        }
        ?>

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <div class="row">
            <div class="col-md-6">
                <label class="control-label" for="customer">Recipient</label>
                <input type="email" name="to" class="form-control" value="" placeholder="Recipient email" required>
            </div>
            <div class="col-md-3">
                <label class="control-label" for="customer">Our Ref</label>
                <input type="text" class="form-control" value="<?= $model->ourRef ?>" readonly>
            </div>
            <div class="col-md-3">
                <label class="control-label" for="customer">Invoice Number</label>
                <input type="text" name="invoiceNumber" class="form-control" value="<?= $model->invoiceNumber ?>" readonly>
            </div>
            <div class="col-md-12">
                <label class="control-label" for="dateRange">Body</label>
                <?= CKEditor::widget([
                    'name' => 'body',
                    'options' => ['rows' => 6],
                    'preset' => 'basic'
                ]) ?>
            </div>
        </div>

        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Send Email'), ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>


        <?php ActiveForm::end(); ?>

    </div>

</div>
