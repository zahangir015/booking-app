<?php

use app\models\Particular;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Voucher */
/* @var $form yii\widgets\ActiveForm */

$particulars = ArrayHelper::map(Particular::findAll(['status' => 1]), 'id', 'title');
?>


<div id="element<?= $row?>">
    <div class="col-md-8">
        <div class="form-group field-inbill-particulars">
            <?= Html::dropDownList('text', 'Inbill[particulars][]',$particulars,['class' => 'form-control']); ?>
            <div class="help-block"></div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group field-inbill-amounts">
            <?= Html::input('text', 'Inbill[amounts][]',null,['class' => 'form-control amount', 'min' => 1]); ?>
            <div class="help-block"></div>
        </div>
    </div>
</div>
