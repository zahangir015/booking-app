<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InbillSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inbill-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ourRef') ?>

    <?= $form->field($model, 'yourRef') ?>

    <?= $form->field($model, 'invoiceNumber') ?>

    <?= $form->field($model, 'date') ?>

    <?php // echo $form->field($model, 'companyId') ?>

    <?php // echo $form->field($model, 'tradeMark') ?>

    <?php // echo $form->field($model, 'particulars') ?>

    <?php // echo $form->field($model, 'amounts') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'currency') ?>

    <?php // echo $form->field($model, 'bankId') ?>

    <?php // echo $form->field($model, 'createdAt') ?>

    <?php // echo $form->field($model, 'createdBy') ?>

    <?php // echo $form->field($model, 'updatedAt') ?>

    <?php // echo $form->field($model, 'updatedBy') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
