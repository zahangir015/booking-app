<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Inbill */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Inbills'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inbill-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'ourRef',
            'yourRef',
            'invoiceNumber',
            'date',
            //'company.name',
            //'company.country',
            [
                'attribute' => 'company',
                'label' => 'Company Name',
                'value' => function($model){
                    return $model->company->name.' - '.$model->company->country;
                },
            ],
            [
                'attribute' => 'bank',
                'label' => 'Bank Name',
                'value' => function($model){
                    return $model->bank->bankName.' - '.$model->bank->accountName;
                },
            ],
            'tradeMark',
            'particulars:ntext',
            'amounts:ntext',
            'total',
            'currency',
            [
                'attribute' => 'status',
                'label' => 'Status',
                'value' => function($model){
                    return ($model->status) ? 'Paid' : 'Unpaid';
                },
            ],
            //'bank.bankName',
            //'bank.accountName',
            'createdAt',
            'createdBy',
            'updatedAt',
            'updatedBy',
        ],
    ]) ?>

</div>
