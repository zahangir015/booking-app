<?php

use app\models\Company;
use app\models\BankAccount;
use app\models\Particular;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Inbill */
/* @var $form yii\widgets\ActiveForm */

$this->registerJs(
    "var ajaxUrl = '" . Yii::$app->request->baseUrl . '/inbill/add-particular' . "';var _csrf='" . Yii::$app->request->getCsrfToken() . "';",
    View::POS_HEAD,
    'url'
);

$this->registerJsFile(
    '@web/js/invoice.js',
    ['depends' => [JqueryAsset::className()]]
);

$companies = ArrayHelper::map(Company::find()->all(), 'id', function ($model) {
    return $model->name . '-' . $model->country;
});

$bankAccounts = ArrayHelper::map(BankAccount::find()->all(), 'id', 'bankName');

$particulars = ArrayHelper::map(Particular::findAll(['status' => 1]), 'id', 'title');
?>

<div class="inbill-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'ourRef')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'yourRef')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'invoiceNumber')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'Enter date ...'],
                'type' => DatePicker::TYPE_INPUT,
                'pluginOptions' => [
                    'autoclose' => true,
                    'format' => 'yyyy-mm-dd',
                ]
            ]);
            ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'companyId')->widget(Select2::classname(), [
                'data' => $companies,
                'language' => 'en',
                'options' => ['placeholder' => 'Select a company ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'tradeMark')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <p class="lead">Particulars</p>
    <hr>
    <div class="row" id="particular">
        <div class="col-md-12">
            <button id="addParticulars" class="btn btn-default"
                    type="button" <?= ($model->isNewRecord) ? 'data-click=2' : 'data-click=' . (count(json_decode($model->particulars)) + 1) ?>>
                Add
            </button>
            <button id="removeParticulars" class="btn btn-danger"
                    type="button" <?= ($model->isNewRecord) ? 'data-click=2' : 'data-click=' . count(json_decode($model->particulars)) ?>>
                Remove
            </button>
        </div>
        <hr>

        <?php
        if ($model->isNewRecord) {
            ?>
            <div id="element">
                <div class="col-md-8">
                    <?= $form->field($model, 'particulars[]')->dropDownList($particulars, ['class' => 'form-control',]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'amounts[]')->textInput(['class' => 'form-control amount',]) ?>
                </div>
            </div>
            <?php
        } else {
            foreach (json_decode($model->particulars) as $key => $particular) {
                $amounts = json_decode($model->amounts);
                $flag = (!$key) ? true : false;
                ?>
                <div id="element<?= ($key + 1) ?>">
                    <div class="col-md-8">
                        <?= $form->field($model, 'particulars[]')->textInput(['value' => $particular])->label((!$key) ? 'Particulars' : false) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'amounts[]')->textInput(['value' => $amounts[$key], 'class' => 'form-control amount'])->label((!$key) ? 'Amounts' : false) ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'total')->textInput(['readOnly' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'currency')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'bankId')->widget(Select2::classname(), [
                'data' => $bankAccounts,
                'language' => 'en',
                'options' => ['placeholder' => 'Select a bank ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            ?>
        </div>

        <div class="col-md-3">
            <?= $form->field($model, 'status')->dropDownList([0 => 'Unpaid', 1 => 'Paid'],['maxlength' => true, 'value' => $model->status]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
