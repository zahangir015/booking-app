<?php

use yii\db\Migration;

/**
 * Handles the creation of table `booking`.
 */
class m180127_192111_create_booking_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('booking', [
            'id' => $this->primaryKey(),
            'destination' => "ENUM('Location 1', 'Location 2', 'Location 3', 'Location 4') NOT NULL",
            'carNumber' => "ENUM('Car 1', 'Car 2', 'Car 3') NOT NULL",
            'bookingTime' => $this->dateTime()->notNull(),
            'returnTime' => $this->dateTime()->notNull(),
            'pickupFrom' => "ENUM('Location 1', 'Location 2', 'Location 3', 'Location 4')",
            'passengers' => $this->integer(),
            'createdAt' => $this->dateTime()->notNull(),
            'createdBy' => $this->integer()->notNull(),
            'updatedAt' => $this->dateTime(),
            'updatedBy' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('booking');
    }
}
