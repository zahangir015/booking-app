<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bank_account`.
 */
class m180403_090136_create_bank_account_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bank_account', [
            'id' => $this->primaryKey(),
            'accountName' => $this->string()->notNull(),
            'accountNumber' => $this->string()->notNull(),
            'bankName' => $this->string()->notNull(),
            'address' => $this->text(),
            'phone' => $this->string(),
            'swiftCode' => $this->string(),
            'createdAt' => $this->dateTime()->notNull(),
            'createdBy' => $this->integer()->notNull(),
            'updatedAt' => $this->dateTime(),
            'updatedBy' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bank_account');
    }
}
