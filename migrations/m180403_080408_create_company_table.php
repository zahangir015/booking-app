<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company`.
 */
class m180403_080408_create_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->unique()->notNull(),
            'address' => $this->string(),
            'country' => $this->string(),
            'city' => $this->string(),
            'contact' => $this->string(),
            'createdAt' => $this->dateTime()->notNull(),
            'createdBy' => $this->integer()->notNull(),
            'updatedAt' => $this->dateTime(),
            'updatedBy' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company');
    }
}
