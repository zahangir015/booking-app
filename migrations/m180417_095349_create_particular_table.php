<?php

use yii\db\Migration;

/**
 * Handles the creation of table `particular`.
 */
class m180417_095349_create_particular_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('particular', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->unique()->notNull(),
            'status' => $this->boolean()->defaultValue(1),
            'createdAt' => $this->dateTime()->notNull(),
            'createdBy' => $this->integer()->notNull(),
            'updatedAt' => $this->dateTime(),
            'updatedBy' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('particular');
    }
}
