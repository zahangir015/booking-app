<?php

use yii\db\Migration;

/**
 * Handles the creation of table `inbill`.
 */
class m180403_101404_create_inbill_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('inbill', [
            'id' => $this->primaryKey(),
            'ourRef' => $this->string()->notNull(),
            'yourRef' => $this->string()->notNull(),
            'invoiceNumber' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'companyId' => $this->integer()->notNull(),
            'tradeMark' => $this->string()->notNull(),
            'particulars' => $this->text()->notNull(),
            'amounts' => $this->text()->notNull(),
            'total' => $this->double()->notNull(),
            'currency' => $this->string()->notNull(),
            'bankId' => $this->integer()->notNull(),
            'status' => $this->boolean()->defaultValue(0)->notNull(),
            'createdAt' => $this->dateTime()->notNull(),
            'createdBy' => $this->integer()->notNull(),
            'updatedAt' => $this->dateTime(),
            'updatedBy' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('inbill');
    }
}
